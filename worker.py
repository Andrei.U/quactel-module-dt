import threading
import time
from threading import Thread

import serial
import serial.tools.list_ports as ports_list

from config import Config


class WorkerError(Exception):
    """Worker class exception"""

    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return '{}, {}'.format(self.__class__, self.message)
        else:
            return '{}, has been raised'


class Worker(Thread):
    status = 'not started'
    connected = False
    gps_enable = False

    def __init__(self, config: Config):
        Thread.__init__(self)
        self.__current_config = config.get_configuration()
        self.__data = {}
        self.stop_event = threading.Event()
        self.is_run = True
        self.dt_output = ''
        if self.__check_ports():
            self.__init()
        else:
            raise WorkerError('Ports existing error')

    def __check_ports(self):
        """check COM port available"""
        gps_flag = com_flag = False
        if len(self.__current_config.keys()):
            for dev in ports_list.comports():
                if dev.device == self.__current_config['port_com']:
                    com_flag = True
                    continue
                if dev.device == self.__current_config['port_gps']:
                    gps_flag = True
                    continue
            if not gps_flag:
                raise WorkerError('Not found port: {}, check you "config.json" file'
                                  .format(self.__current_config['port_gps']))
            if not com_flag:
                raise WorkerError('Not found port: {}, check you "config.json" file'
                                  .format(self.__current_config['port_com']))
            return True

    def __init(self):
        self.com_config = {
            "port": self.__current_config['port_com'],
            "baudrate": self.__current_config['baudrate'],
            "bytesize": self.__current_config['bytesize'],
            "parity": serial.PARITY_NONE,
            "stopbits": self.__current_config['stopbit'],
            "timeout": self.__current_config['timeout']
        }
        self.connect_com = Connection(**self.com_config)
        self.__gps_enable()
        if not self.gps_enable:
            raise WorkerError('Not passable activate GPS with AT Command')
        self.gps_config = {
            "port": self.__current_config['port_gps'],
            "baudrate": self.__current_config['baudrate'],
            "bytesize": self.__current_config['bytesize'],
            "parity": serial.PARITY_NONE,
            "stopbits": self.__current_config['stopbit'],
            "timeout": self.__current_config['timeout']
        }

    def stop(self):
        self.is_run = False
        self.stop_event.set()

    def run(self):
        gps_task = GpsReaderThread(self.gps_config)
        com_task = ComReaderThread(self.com_config)
        gps_task.start()
        com_task.start()
        while self.is_run:
            gps_task.join(0.001)
            gps_point = gps_task.get_location()
            com_task.join(0.001)
            com_point = com_task.get_rf_data()
            if gps_point and com_point:
                self.__data.update(dict(gps_point, **com_point))
        gps_task.stop()
        com_task.stop()

    def __gps_enable(self):
        self.connect_com.open()
        command = Command(self.connect_com.ser)
        self.gps_enable = command.gps_enable()
        self.connect_com.close()

    def get_data(self):
        return self.__data


class Connection:
    """Connection class for Quectel interfaces NMEA and AT+COM"""
    def __init__(self, **kwargs):
        self.config = kwargs
        self.ser = None

    def open(self):
        return self.__enter__()

    def close(self):
        if self.ser and self.ser.isOpen():
            self.ser.close()

    def is_open(self):
        if self.ser:
            return self.ser.isOpen()
        return None

    def __enter__(self):
        if not self.ser or not self.ser.isOpen():
            self.ser = serial.Serial(**self.config)
        else:
            self.ser.close()
            self.ser = serial.Serial(**self.config)
        return self.ser

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.ser and self.ser.isOpen():
            self.ser.close()
        if exc_val:
            raise


class Command:

    def __init__(self, connect):
        self.__connect = connect

    def __run_command(self, command, timeout=0.1):
        try:
            b = str.encode(command + "\r\n", encoding="ascii")
            self.__connect.write(b)
            self.__connect.flush()
            time.sleep(timeout)
            vc = self.__connect.read_all()
            data = vc.decode(encoding="ascii")
            return data.replace('\n', '').replace('\r', '')
        except Exception as e:
            print(str(e))
        return ""

    def network_info(self):
        data = self.__run_command('AT+QNWINFO')
        data = data.split(',')
        i = 2
        text = ""
        for i in range(i, len(data) - 1):
            text += (data[i] + ',')
            i += 1
        return text

    def serving_cell_info(self):
        data = self.__run_command('AT+QENG="servingcell"')
        if len(data.split(',')) > 4:
            data = data.split(',')
            i = 2
            text = ''
            for i in range(i, len(data) - 1):
                text += (data[i] + ',')
                i += 1
            return text[:-1]
        else:
            return ''

    def gps_enable(self):
        data = self.__run_command('AT+QGPS=1', 0.2)
        if data.replace('AT+QGPS=1', '') == 'OK' or '504' in data:
            return True
        else:
            return data

    def gps_disable(self):
        data = self.__run_command('AT+QGPSEND', 0.2)
        if data.replace('AT+QGPSEND', '') == 'OK':
            return True
        else:
            return data

    def neighbour_cell_info(self):
        data = self.__run_command('AT+QENG="neighbourcell"')
        text = '{'
        i = 1
        data = data[:-2]
        data = data.split('+QENG: ')
        for i in range(i, len(data)):
            y = 0
            text += (data[i] + '\n')
        return text[:-1] + '}'.replace('OK', '')

    def get_ip(self):
        data = self.__run_command('AT+CGPADDR=1', 0.3)
        if len(data.split(',')):
            return data.split(',')[1][:-2]
        else:
            return None

    # TODO Need to correct function

    """  
    def scanning(self):
        data = self.__run_command('AT+QCOPS=7', 70)

    def get_ip(self):
        return self.__run_command('AT+QIACT?', 0.1), self.__run_command('AT+CGPADDR=1', 0.3)

    def get_time(self):
        data = self.__run_command('AT+QLTS=2', 0.3)
        return data

    def reg_status(self):
        self.__run_command('AT+CGREG=2')
        self.__run_command('AT+CGREG?')
        self.__run_command('AT+CGREG=0')

    def pdp_counter(self):
        self.__run_command('AT+QGDCNT?')

    def pdp_counter_reset(self):
        self.__run_command('AT+QGDCNT=0')

    def battery_status(self):
        return self.__run_command('AT+CBC')

    def ping(self, host, timeout, ping_count):
        com = 'AT+QPING=1,"'+host+'",'+timeout+','+ping_count
        return self.__run_command(com, 1)

    def pdp_activate(self, apn_name: str):
        return self.__run_command('AT+QICSGP=1,1,"'+apn_name+'","","",0"', 1),\
               self.__run_command("AT+QIACT=1"),\
               self.__run_command("AT+QIACT?")

    def pdp_deactivate(self):
        return self.__run_command('AT+QIDEACT=1', 1)
    """


class GpsReaderThread(Thread):
    """GPS Thread class"""

    def __init__(self, config):
        Thread.__init__(self)
        self.connect = Connection(**config)
        self.connect.open()
        self.name = 'gps thread'
        self.stop_event = threading.Event()
        self.is_run = True
        self.__location = {}

    def stop(self):
        self.is_run = False
        self.stop_event.set()

    def get_location(self):
        if self.__location:
            return self.__location

    def run(self):
        def gps_parser(my_list):
            time_utc = '{}:{}:{}'.format(my_list[1][:2], my_list[1][2:4], my_list[1][4:])
            lon = int(my_list[3][:2]) + float(my_list[3][2:]) / 60
            lat = int(my_list[5][:3]) + float(my_list[5][3:]) / 60
            speed_ms = round(float(my_list[7]) * 0.514444, 2)
            track_digress = float(my_list[8])
            date = '{}/{}/{}'.format(my_list[9][:2], my_list[9][2:4], my_list[9][4:])
            return {
                "utc": time_utc,
                "date": date,
                "lon": round(lon, 6),
                "lat": round(lat, 6),
                "speed_ms": speed_ms,
                "track_digress": track_digress
            }

        def alt_parser(my_list):
            sat_count = my_list[7]
            alt = my_list[9]
            if alt.isalnum():
                alt = float(alt)
            if sat_count.isalnum():
                sat_count = float(sat_count)
            if alt:
                return {
                    "alt": alt,
                    "sat_count": sat_count
                }
            else:
                return {}

        targets_line = ('$GPRMC', '$GPGGA')
        port = self.connect.ser
        while self.is_run:
            data = port.read_all()
            data = data.decode().replace('\r', '')
            for line in data.split('\n'):
                if line.split(',')[0] not in targets_line:
                    continue
                else:
                    if line.split(',')[0] == targets_line[0]:
                        if line.split(',')[2] == 'A':
                            self.__location.update(gps_parser(line.split(',')))
                    else:
                        self.__location.update(alt_parser(line.split(',')))
            time.sleep(0.5)
        self.connect.close()


class ComReaderThread(Thread):

    def __init__(self, config):
        Thread.__init__(self)
        self.connect = Connection(**config)
        self.connect.open()
        self.command = Command(self.connect.ser)
        self.is_run = True
        self.__data = {}
        self.stop_event = threading.Event()
        self.name = 'AT+Command Thread'

    def run(self):
        def get_rf(rf):
            unavailable = -999
            bandwidth = (1.4, 3, 5, 10, 15, 20)
            return {
                "is_tdd": rf[1],
                "mmc": rf[2] if len(rf[2]) else '',
                "mnc": rf[3] if len(rf[3]) else '',
                "cell_id": rf[4] if len(rf[4]) else '',
                "pci": rf[5] if rf[5].isalnum() else unavailable,
                "earfcn": rf[6] if rf[6].isalnum() else unavailable,
                "freq_band_id": rf[7] if rf[7].isalnum() else unavailable,
                "ul_bandwidth": bandwidth[int(rf[8])] if rf[8].isalnum() else unavailable,
                "dl_bandwidth": bandwidth[int(rf[9])] if rf[9].isalnum() else unavailable,
                "tac": int(rf[10]) if rf[10].isalnum() else unavailable,
                "rsrp": int(rf[11]) if rf[11][1:].isalnum() else unavailable,
                "rsrq": int(rf[12]) if rf[12][1:].isalnum() else unavailable,
                "sinr": int(rf[13]) if rf[13][1:].isalnum() else unavailable,
                "srxlev": int(rf[14]) if rf[14][1:].isalnum() else unavailable
            }

        while self.is_run:
            self.__data = get_rf(self.command.serving_cell_info().split(','))

    def get_rf_data(self):
        return self.__data

    def stop(self):
        self.is_run = False
        self.stop_event.set()
