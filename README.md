Python **"LTE Drive Test"** application for Quectel modules

Supported EC20, EC25, EP06 modules

**Setup:**
1. install requirements: `pip install -r requirements.txt`
2. run script in stand alone with offline mode:` "python -m offline.py"`
3. run script from any other script: `from offline import OfflineTask; OfflineTask().run()`
