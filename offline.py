import os
import os.path
import time
from os.path import isfile

from config import Config
from worker import Worker


class OfflineTask:
    def __init__(self):
        config = Config.load_config()
        self.work_dir = os.path.join(os.path.abspath(os.getcwd()), 'data')
        self.worker = Worker(config)
        self.new_file = True
        self.is_run = None
        if 'data' not in os.listdir(os.path.abspath(os.getcwd())):
            os.mkdir(os.path.join(os.curdir, 'data'))
        self.work_dir = os.path.join(os.curdir, 'data')
        self.file_path = os.path.join(self.work_dir,
                                      '{}_{}.csv'.format(time.strftime("%d-%m-%Y"), time.strftime("%H%M%S")))

    def run(self):
        self.worker.start()
        point_hash = 0
        self.is_run = True
        if isfile(self.file_path):
            self.new_file = False
        with open(self.file_path, 'a') as file:
            while self.is_run:
                self.worker.join(0.02)
                point = self.worker.get_data()
                if point_hash != hash(frozenset(point.items())):
                    point_hash = hash(frozenset(point.items()))
                    if len(point.items()):
                        line = self.convert_to_csv(**point)
                        file.write(line)

        self.worker.stop()

    def convert_to_csv(self, **point):
        current_line = ''
        if self.new_file:
            for key in point.keys():
                current_line += '{},'.format(key)
            self.new_file = False
            current_line = current_line[:-1] + '\n'
        for item in point.items():
            current_line += '{},'.format(item[1])
        current_line = current_line[:-1] + '\n'
        return current_line

    def stop(self):
        self.is_run = False


class OfflineTaskError(Exception):
    """Configuration class exception"""

    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return '{}, {}'.format(self.__class__, self.message)
        else:
            return '{}, has been raised'


if __name__ == '__main__':
    OfflineTask().run()
