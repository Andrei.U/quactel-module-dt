import json
import os
from os.path import isfile


class Config(object):
    __baud_rate: int = 115200
    __byte_size: int = 8
    __stop_bit: int = 1
    __timeout: int = 0
    __config_path = os.path.join(os.getcwd(), 'config.json')

    @staticmethod
    def load_config():
        if not isfile(Config.__config_path):
            raise ConfigError('Config file not found')
        else:
            try:
                with open(Config.__config_path, 'r') as file:
                    return json.loads(file.read(), object_hook=Config.object_decoder)
            except Exception as e:
                raise e

    @staticmethod
    def object_decoder(obj):
        return Config(obj['port_com'],
                      obj['port_gps'],
                      obj['online_mode'],
                      obj['host'],
                      obj['port'], )

    def __init__(self, port_com: str, port_gps: str, online_mode: bool, host: str, port: str):
        self.port_com = port_com
        self.port_gps = port_gps
        self.online_mode = online_mode
        self.host = host
        self.port = port

    def check_config(self):
        if self.port_com is '':
            print('COM PORT not correct')
            return False
        if self.port_gps is '':
            print('GPS Port not correct')
            return False
        return True

    def get_configuration(self):
        if not self.check_config():
            return {}
        else:
            if self.online_mode:
                return {'port_com': self.port_com,
                        'port_gps': self.port_gps,
                        'online_mode': self.online_mode,
                        'host': self.host,
                        'port': self.port,
                        'baudrate': self.__baud_rate,
                        'bytesize': self.__byte_size,
                        'stopbit': self.__stop_bit,
                        'timeout': self.__timeout}
            else:
                return {'port_com': self.port_com,
                        'port_gps': self.port_gps,
                        'online_mode': self.online_mode,
                        'baudrate': self.__baud_rate,
                        'bytesize': self.__byte_size,
                        'stopbit': self.__stop_bit,
                        'timeout': self.__timeout}


class ConfigError(Exception):
    """Configuration class exception"""

    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return '{}, {}'.format(self.__class__, self.message)
        else:
            return '{}, has been raised'
