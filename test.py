import json
import os
import time

import serial

from config import Config
from worker import ComReaderThread
from worker import Command
from worker import Connection
from worker import GpsReaderThread


def timeit(method):
    def timed(*args, **kwargs):
        ts = time.time()
        result = method(*args, **kwargs)
        te = time.time()
        if 'log_time' in kwargs:
            name = kwargs.get('log_name', method.__name__.upper())
            kwargs['log_time'][name] = round((te - ts) * 1000, 6)
        else:
            print('{}, {} ms'.format(method.__name__, round((te - ts) * 1000, 6)))
        return result

    return timed


class ConfigTests:
    config_path = os.path.join(os.getcwd(), 'config.json')

    def config_test1(self):
        """Test #1\nDict test"""
        with open(self.config_path, 'r') as file:
            config = (json.loads(file.read(), object_hook=Config.object_decoder))
            print("OK" if type(config.get_configuration()) is dict else "FAIL")


class ConnectTest:
    def __init__(self):
        self.config_path = os.path.join(os.getcwd(), 'config.json')
        with open(self.config_path, 'r') as file:
            self.current_config: Config = (json.loads(file.read(), object_hook=Config.object_decoder))
        self.config = self.current_config.get_configuration()
        conf = {
            "port": self.config['port_com'],
            "baudrate": self.config['baudrate'],
            "bytesize": self.config['bytesize'],
            "parity": serial.PARITY_NONE,
            "stopbits": self.config['stopbit'],
            "timeout": self.config['timeout']
        }
        self.connect = Connection(**conf)

    def test_connection(self):
        """Connection test: Open"""
        self.connect.open()
        print('OK' if self.connect.is_open() else 'FAIL')
        self.connect.close()

    def test_connection_refuse(self):
        """Connection refuse test"""
        print('Closed test:', end=' ')
        print('OK' if not self.connect.is_open() else 'FAIL')
        self.connect.open()
        print('Opening test:', end=' ')
        print('OK' if self.connect.is_open() else 'FAIL')
        self.connect.close()

    def test_rsrp(self):
        """Commands Tests:"""
        print('Network Info:', end=' ')
        with self.connect as connect:
            command: Command = Command(connect)
            print(command.network_info())

        print('Serving Cell Info:', end=' ')
        with self.connect as connect:
            command: Command = Command(connect)
            print(command.serving_cell_info())

        print('Neighbour Cell Info:', end=' ')
        with self.connect as connect:
            command: Command = Command(connect)
            print(command.neighbour_cell_info())

        print('GPS Enable:', end=' ')
        with self.connect as connect:
            command: Command = Command(connect)
            print(command.gps_enable())

        print('GPS Disable:', end=' ')
        with self.connect as connect:
            command: Command = Command(connect)
            print(command.gps_disable())

        print('Get IP:', end=' ')
        with self.connect as connect:
            command: Command = Command(connect)
            print(command.get_ip())


class GPSTest:
    def __init__(self):
        self.config_path = os.path.join(os.getcwd(), 'config.json')
        with open(self.config_path, 'r') as file:
            self.current_config: Config = (json.loads(file.read(), object_hook=Config.object_decoder))
        self.config = self.current_config.get_configuration()
        self.conf_gps = {
            "port": self.config['port_gps'],
            "baudrate": self.config['baudrate'],
            "bytesize": self.config['bytesize'],
            "parity": serial.PARITY_NONE,
            "stopbits": self.config['stopbit'],
            "timeout": self.config['timeout']
        }
        self.conf_com = {
            "port": self.config['port_com'],
            "baudrate": self.config['baudrate'],
            "bytesize": self.config['bytesize'],
            "parity": serial.PARITY_NONE,
            "stopbits": self.config['stopbit'],
            "timeout": self.config['timeout']
        }

    def gps(self):
        """GPSTests:"""
        print('Network Info:', end=' ')
        TARGET_LINE = '$GPRMC'
        with Connection(**self.conf_com) as port:
            print(Command(port).gps_enable())
        with Connection(**self.conf_gps) as port:
            for i in range(600):
                bin_data = port.readall()
                print(bin_data.decode().replace('\r', '') if len(bin_data) else 'None')
                print('========Count {}========'.format(i))
                time.sleep(1)

    def gps_reader_test(self):
        """test for gps reader"""
        with Connection(**self.conf_com) as port:
            print(Command(port).gps_enable())
        gps_reader = GpsReaderThread(self.conf_gps)
        gps_reader.start()
        print('GPS reader test:', end=' ')
        for i in range(10):
            gps_reader.join(0.1)
            loc = gps_reader.get_location()
            if loc:
                print(loc, 'OK')
                gps_reader.stop()
                return
        gps_reader.stop()
        print('FAIL')


class ComThreadTest:

    def __init__(self):
        self.config_path = os.path.join(os.getcwd(), 'config.json')
        with open(self.config_path, 'r') as file:
            self.current_config: Config = (json.loads(file.read(), object_hook=Config.object_decoder))
        self.config = self.current_config.get_configuration()
        self.conf_com = {
            "port": self.config['port_com'],
            "baudrate": self.config['baudrate'],
            "bytesize": self.config['bytesize'],
            "parity": serial.PARITY_NONE,
            "stopbits": self.config['stopbit'],
            "timeout": self.config['timeout']
        }
        self.com_reader = ComReaderThread(self.conf_com)

    def test_rf_command(self):
        """RF data test function"""
        self.com_reader.start()
        print('RF data test: ', end=' ')
        for i in range(10):
            self.com_reader.join(0.1)
            if self.com_reader.get_rf_data():
                print("OK")
                print(self.com_reader.get_rf_data())
                self.com_reader.stop()
                return
        print('FAIL')
        self.com_reader.stop()


@timeit
def tester(foo):
    print('================================')
    print(foo.__doc__)
    return foo()


tester(ConfigTests().config_test1)

tester(ConnectTest().test_connection)

tester(ConnectTest().test_connection_refuse)

tester(ConnectTest().test_rsrp)

tester(GPSTest().gps_reader_test)

tester(ComThreadTest().test_rf_command)

# config_path = os.path.join(os.getcwd(), 'config.json')
# with open(config_path, 'r') as file:
#     worker = Worker(json.loads(file.read(), object_hook=Config.object_decoder))
# worker.start()
# while True:
#     worker.join(0.15)
#     print(worker.get_data())

# $GPGGA,101102.84,3158.175618,N,03450.308711,E,1,07,1.7,67.6,M,19.0,M,,*50
# tester(GPSTest().gps)
# def test(**kwargs):
#     text = '$GPRMC,142704.00,A,3158.175722,N,03450.307531,E,0.0,124.8,170520,3.0,E,A*36'
#     my_list = text.split(',')
#     time = '{}:{}:{}'.format(my_list[1][:2], my_list[1][2:4], my_list[1][4:])
#     lon = int(my_list[3][:2]) + float(my_list[3][2:]) / 60
#     lat = int(my_list[5][:3]) + float(my_list[5][3:]) / 60
#     speed_kn = float(my_list[7])
#     track_digress = float(my_list[8])
#     date = '{}/{}/{}'.format(my_list[9][:2], my_list[9][2:4], my_list[9][4:])
#     data = (time, date, round(lon, 6), round(lat, 6), speed_kn, track_digress)
#     print(data)

# def test(**kwargs):
#     text = '$GPGGA,101102.84,3158.175618,N,03450.308711,E,1,07,1.7,67.6,M,19.0,M,,*50'
#     my_list = text.split(',')
#     sat_count = my_list[7]
#     alt = my_list[9]
#     print(alt, sat_count)
#
#
# test()
